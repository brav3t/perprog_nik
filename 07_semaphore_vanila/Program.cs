﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _07_semaphore_vanila
{
    class Program
    {
        static SemaphoreSlim sem = new SemaphoreSlim(0, 3);

        static void Main(string[] args)
        {
            List<Task> tasks = new List<Task>();

            for (int i = 0; i < 10; i++)
            {
                int db = 1;

                int j = i + 1;
                tasks.Add(new Task(() => 
                {
                    sem.Wait();
                    Thread.Sleep(100);
                    ToConsole($"Task elvégezve");
                }, TaskCreationOptions.LongRunning));
            }

            tasks.Add(new Task(() =>
            {
                int db = 3;
                foreach (var t in tasks)
                {
                    sem.Release(db);
                    ToConsole($"Semafor átengedett {db} taskot");
                    Thread.Sleep(2000);
                }
            }));

            foreach (var t in tasks)
            {
                t.Start();
            }

            Console.ReadKey();
        }

        static object lockObject = new object();
        static void ToConsole(string message)
        {
            lock (lockObject)
            {
                Console.WriteLine(message);
            }
        }
    }
}
