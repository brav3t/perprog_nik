﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01
{
    class Lo
    {
        static Random rnd = new Random();
        string[] nevek = { "Lucky", "Vágta", "Sebes", "Szélvész", "Marcona", "Villám", "Pata", "Csillag", "Öszvér", "Patkó", "RainbowDash" };
        static readonly int CEL = 70;

        DateTime start;
        TimeSpan futamIdo;
        public bool CelbaErt { get { return tav == CEL; } }

        string nev;
        Zsoke zsoke;
        int szerencse;
        int tav;
        public int TEMPO { get; private set; }

        public Lo(Zsoke zsoke)
        {
            this.nev = nevek[rnd.Next(0, nevek.Length)];
            this.zsoke = zsoke;
            this.szerencse = rnd.Next(1, 100);
            this.tav = 0;

            if (this.nev == "RainbowDash") Interlocked.Increment(ref szerencse);

            TEMPO = (int)Math.Round(100 * szerencse/100 * zsoke.Magassag/(double)Zsoke.IDEALISMAGASSAG * zsoke.Tomeg/Zsoke.IDEALISSULY);
        }

        public void Versenyez()
        {
            if (tav == 0)
            {
                start = DateTime.Now;
            }
            Interlocked.Increment(ref tav);
            if (CelbaErt)
            {
                futamIdo = start - DateTime.Now;
            }            
        }

        public override string ToString()
        {
            return nev + ":\t" + "x".PadLeft(tav, '-') + "\n";
        }
    }
}
