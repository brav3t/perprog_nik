﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01
{
    class Zsoke
    {
        static Random rnd = new Random();
        public static readonly int IDEALISMAGASSAG = 150, IDEALISSULY = 45;

        public int Magassag { get; set; }
        public int Tomeg { get; set; }

        public Zsoke()
        {
            Magassag = rnd.Next(140, 165);
            Tomeg = rnd.Next(40, 60);
        }
    }
}
