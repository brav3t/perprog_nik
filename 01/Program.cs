﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01
{
    class Program
    {
        static void Main(string[] args)
        {
            int versenyzokSzama = 10;
            bool mindenkiCelbaErt = false;

            Zsoke[] zsokek = new Zsoke[versenyzokSzama];

            for (int i = 0; i < zsokek.Length; i++)
            {
                zsokek[i] = new Zsoke();
            }

            Lo[] lovak = new Lo[versenyzokSzama];

            for (int i = 0; i < lovak.Length; i++)
            {
                lovak[i] = new Lo(zsokek[i]);
            }

            List<Task> tasks = new List<Task>();

            foreach (Lo lo in lovak)
            {
                tasks.Add(new Task(() =>
                {
                    if (!lo.CelbaErt)
                    {
                        Thread.Sleep(lo.TEMPO);
                        lo.Versenyez();
                    }
                }, TaskCreationOptions.LongRunning));
            }

            Task.WhenAll(tasks).ContinueWith(ts =>
            {
                mindenkiCelbaErt = true;
            });

            foreach (var t in tasks)
            {
                t.Start();
            }

            Task naplo = new Task(() =>
            {
                while (!mindenkiCelbaErt)
                {
                    Console.Clear();
                    foreach (Lo l in lovak)
                    {
                        Console.WriteLine(l);
                        //Thread.Sleep(100);
                    }
                }

            }, TaskCreationOptions.LongRunning);

            naplo.Start();

            Console.ReadLine();
        }
    }
}
