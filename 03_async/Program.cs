﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _03_async
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello ");
            Console.WriteLine(Valasz().Result);
            
            Console.WriteLine("World!");

            Console.ReadKey();
        }

        static async Task<string> Valasz()
        {
            await Task.Run(() => Thread.Sleep(1000));
            return " World!! ";
        }
    }
}
