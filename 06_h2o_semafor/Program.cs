﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _06_h2o_semafor
{
    class Program
    {
        static Random rnd = new Random();
        static object lockObject = new object();
        static SemaphoreSlim sem = new SemaphoreSlim(0);

        static int H, O;

        static void Main(string[] args)
        {
            List<Task> tasks = new List<Task>();

            for (int i = 0; i < 2; i++)
            {
                tasks.Add(new Task(() =>
                {
                    while (true)
                    {
                        Thread.Sleep(RandomWait());
                        GenerateH();
                    }
                }));
            }

            tasks.Add(new Task(()=> 
            {
                while (true)
                {
                    Thread.Sleep(RandomWait());
                    GenerateO();
                }

            }));

            foreach (var t in tasks)
            {
                t.Start();
            }

            Console.ReadKey();
        }

        static int RandomWait()
        {
            int r;
            lock(lockObject)
            {
                r = rnd.Next(1000, 10001);
            }
            return r;
        }

        static void GenerateH()
        {
            Interlocked.Increment(ref H);
            Console.WriteLine("H generated");
         
        }

        static void GenerateO()
        {
            Interlocked.Increment(ref O);
            Console.WriteLine("O generated");
            if (H >= 2)
            {
                    
            }
        }
    }
}
