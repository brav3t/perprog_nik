﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _04_lock
{
    class Program
    {
        static Random rnd = new Random();
        //static object lockObject = new object();

        static void Main(string[] args)
        {
            List<Generator> generators = new List<Generator>();

            for (int i = 0; i < 120; i++)
            {
                generators.Add(new Generator(i, (ConsoleColor)rnd.Next(0,10)));
            }
            
            List<Task> tasks = new List<Task>();
            foreach (var g in generators)
            {
                tasks.Add(new Task(() => 
                {
                    while (true)
                    {
                        g.Falldown();
                        g.ToConsole();
                        //g.ToConsoleLocked();
                        //ToConsole(g.Left, g.Top, g.Color, rnd.Next(0, 9));
                    }
                }, TaskCreationOptions.LongRunning));
            }

            foreach (var t in tasks)
            {
                t.Start();
            }

            Console.ReadKey();
        }
        
        //static void ToConsole(int left, int top, ConsoleColor color, int message)
        //{
        //    lock(lockObject)
        //    {
        //        Console.SetCursorPosition(left, top);
        //        Console.ForegroundColor = color;
        //        Console.Write(message);
        //    }
        //}
    }

    class Generator
    {
        static Random rnd = new Random();
        static object lockObject = new object();

        int top;

        public Generator(int left, ConsoleColor color)
        {
            this.Top = 0;
            this.Left = left;
            this.Color = color;
        }

        public int Left { get; private set; }
        public int Top { get => top; private set => top = value; }
        public ConsoleColor Color { get; private set; }

        public void Falldown()
        {
            Thread.Sleep(rnd.Next(100, 1000));
            Interlocked.Increment(ref top);
        }

        public void ToConsole()
        {
            Console.SetCursorPosition(Left, Top);
            Console.ForegroundColor = Color;
            Console.Write(rnd.Next(0, 10));
        }

        public void ToConsoleLocked()
        {
            lock (lockObject)
            {
                Console.SetCursorPosition(Left, top);
                Console.ForegroundColor = Color;
                Console.Write(rnd.Next(0, 10));
            }
        }
    }

}
