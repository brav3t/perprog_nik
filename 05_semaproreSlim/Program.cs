﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _05_semaproreSlim
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] urls = new string[]
            {
                "http://users.nik.uni-obuda.hu/perprog/ea/00_kovetelmenyek.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/01_elvi_alapok.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/02_parhuzamos_tervezes.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/03_szinkronizacio.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/04_holtpont.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/05_konkurens_adatszerkezetek.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/06_tervezesi_mintak.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/07_parallel_sum_prefix_scan.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/08_sorting.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/09_mpi.pdf",
                "http://users.nik.uni-obuda.hu/perprog/ea/10_grafbejaras.pdf"
            };

            List<Task> tasks = new List<Task>();

            foreach (var link in urls)
            {
                tasks.Add(new Task(() => 
                {
                    new Downloader().Download(link);
                }, TaskCreationOptions.LongRunning));
            }

            foreach (var t in tasks)
            {
                t.Start();
            }

            Task.WhenAll(tasks).ContinueWith(x => Console.WriteLine("All download is complete!"));

            Console.ReadKey();
        }
    }

    class Downloader
    {
        static SemaphoreSlim sem = new SemaphoreSlim(2);

        public Downloader()
        {
            this.Path = "files/";
        }

        public string Url { get; set; }
        public string Path { get; set; }
        public string FileName { get; private set; }

        public void Download(string url)
        {
            this.FileName = System.IO.Path.GetFileName(url);
            this.Url = url;

            Console.WriteLine($"{FileName} added to download queue");

            sem.Wait();

            Console.WriteLine($"Started downloading: {FileName}");

            using (WebClient wc = new WebClient())
            {
                try
                {
                    wc.DownloadFile(url, Path + FileName);
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error in download of: {FileName} - {e.GetBaseException().Message}");   
                }
            }
            Console.WriteLine($"Finished downloading: {FileName}");

            sem.Release();
        }
    }
}
