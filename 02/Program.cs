﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _02
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Dice> rolls = new List<Dice>();

            for (int i = 0; i < 10; i++)
            {
                rolls.Add(new Dice(i));
            }

            List<Task> tasks = new List<Task>();

            foreach (var r in rolls)
            {
                tasks.Add(new Task(() => 
                {
                    while (true)
                    {
                        Thread.Sleep(100);
                        r.Roll();
                    }
                }, TaskCreationOptions.LongRunning));
            }

            foreach (var t in tasks)
            {
                t.Start();
            }

            Task display = new Task(() => 
            {
                while (true)
                {
                    Console.Clear();
                    foreach (var r in rolls)
                    {
                        Console.WriteLine(r);
                        Thread.Sleep(7);
                    }
                }

            },TaskCreationOptions.LongRunning);

            display.Start();

            Console.ReadKey();
        }
    }

    class Dice
    {
        static Random rnd = new Random();

        public Dice(int id)
        {
            this.ID = id;
        }

        public int ID { get; private set; }
        public int Number { get; private set; }

        public void Roll()
        {
            this.Number = rnd.Next(1, 6);
        }

        public override string ToString()
        {
            return ID + "+".PadLeft(Number, '.');
        }
    }
}
