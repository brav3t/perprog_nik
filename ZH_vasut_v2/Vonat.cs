﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZH_vasut_v2
{
    class Vonat
    {
        static Random rnd = new Random();
        protected string tipus;

        public Vonat(int aktualisAllomas, int irany = 1) // 1 = Siófok felé
        {
            tipus = "Vo";
            this.Irany = irany;
            this.AktualisAllomas = aktualisAllomas;
            this.ElozoAllomas = aktualisAllomas;
            this.KovetkezoAllomas = aktualisAllomas + Irany;
        }

        public int Irany { get; set; }
        public int FelLeSzallasiIdo { get => 2000; }
        public int AllomasKoztiMenetIdo { get => 5000; }
        public int Keses { get => rnd.Next(0, 2001); }
        public int ElozoAllomas { get; set; }
        public int AktualisAllomas { get; set; }
        public int KovetkezoAllomas { get; set; }

        public void Kozlekedik()
        {
           
        }

        public override string ToString()
        {
            if (AktualisAllomas == -1)
            {
                if (Irany == 1)
                {
                    return $"{tipus}->({KovetkezoAllomas})";
                }
                if (Irany == -1)
                {
                    return $"({KovetkezoAllomas})<-{tipus}";
                }
            }
            return $"{tipus}({AktualisAllomas})";
        }
    }

    class SzemelyVonat : Vonat
    {
        public SzemelyVonat(int aktualisAllomas, int irany = 1) : base(aktualisAllomas, irany)
        {
            tipus = "Sz";
        }
    }

    class ExpresszVonat : Vonat
    {
        public ExpresszVonat(int aktualisAllomas, int irany = 1) : base(aktualisAllomas, irany)
        {
            tipus = "Ex";
        }
    }
}
