﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZH_vasut_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            Allomas[] allomasok = new Allomas[10];
            for (int i = 0; i < allomasok.Length; i++)
            {
                allomasok[i] = new Allomas();
            }

            List<Vonat> vonatok = new List<Vonat>()
            {
                new SzemelyVonat(0),
                new SzemelyVonat(0),
                new SzemelyVonat(0)
                //new ExpresszVonat(0),
                //new ExpresszVonat(9, -1)

            };

            List<Task> kozlekedes = new List<Task>();
            foreach (var vonat in vonatok)
            {
                kozlekedes.Add(new Task(() => 
                {
                    while (true)
                    {
                        
                    }
                }, TaskCreationOptions.LongRunning));
            }

            kozlekedes.Add(new Task(() => 
            {
                ToConsole(allomasok, vonatok);
            }, TaskCreationOptions.LongRunning));

            foreach (var t in kozlekedes)
            {
                t.Start();
            }

            Console.ReadKey();
        }

        static void ToConsole(Allomas[] allomasok, List<Vonat> vonatok)
        {
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                //for (int i = 0; i < allomasok.Length; i++)
                //{
                //    sb.Append(allomasok[i].ToString().PadRight(10));
                //}
                //sb.AppendLine();
                foreach (var v in vonatok)
                {
                    if (v.AktualisAllomas == -1)
                    {
                        sb.AppendLine(v.ToString());
                    }
                    else
                    {
                        sb.AppendLine(v.ToString());
                    }
                }
                Console.WriteLine(sb);
                Thread.Sleep(20);
                sb.Clear();
                Console.Clear();
            }
        }
    }
}
