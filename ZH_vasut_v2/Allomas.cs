﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZH_vasut_v2
{
    class Allomas
    {
        static string[] allomasNevek = new string[10]
        {
            "Budapest",
            "Érd",
            "Velence",
            "Gárdony",
            "Székesfehérvár",
            "Szabadbattyán",
            "Polgárdi",
            "Lepsény",
            "Balatonaliga",
            "Siófok"
        };

        static SemaphoreSlim[] sorompok = new SemaphoreSlim[9]
        {
            new SemaphoreSlim(2,2),
            new SemaphoreSlim(2,2),
            new SemaphoreSlim(2,2),
            new SemaphoreSlim(2,2),
            new SemaphoreSlim(1,1),
            new SemaphoreSlim(1,1),
            new SemaphoreSlim(1,1),
            new SemaphoreSlim(1,1),
            new SemaphoreSlim(1,1)
        };

        static int ID = 0;

        int id;

        ConcurrentQueue<Vonat> varakozoExpresszVonatok;
        ConcurrentQueue<Vonat> varakozoSzemelyVonatok;

        public bool ExpresszMegallo { get; }

        public Allomas()
        {
            this.id = ID;
            ID++;

            if (id == 0 || id == 4 || id == 7 || id == 9)
            {
                this.ExpresszMegallo = true;
            }
            else
            {
                this.ExpresszMegallo = false;
            }

            varakozoExpresszVonatok = new ConcurrentQueue<Vonat>();
            varakozoSzemelyVonatok = new ConcurrentQueue<Vonat>();
        }

        public void VonatErkezik(Vonat erkezoVonat)
        {
            sorompok[erkezoVonat.ElozoAllomas].Release();

            erkezoVonat.AktualisAllomas = this.id;

            if (id == 0 && erkezoVonat.Irany == -1 || id == 9 && erkezoVonat.Irany == 1)
            {
                erkezoVonat.Irany *= -1;
            }

            erkezoVonat.KovetkezoAllomas = id + erkezoVonat.Irany;

            if (erkezoVonat is SzemelyVonat)
            {
                varakozoSzemelyVonatok.Enqueue(erkezoVonat);
                Thread.Sleep(erkezoVonat.FelLeSzallasiIdo);
            }

            if (erkezoVonat is ExpresszVonat && this.ExpresszMegallo)
            {
                varakozoExpresszVonatok.Enqueue(erkezoVonat);
                Thread.Sleep(erkezoVonat.FelLeSzallasiIdo);
            }
        }

        public void VonatIndul(Vonat induloVonat)
        {
            if (induloVonat is ExpresszVonat && ExpresszMegallo)
            {
                while (!varakozoExpresszVonatok.TryDequeue(out induloVonat))
                {
                }
            }
            else if (induloVonat is SzemelyVonat)
            {
                while (!varakozoExpresszVonatok.IsEmpty)
                {
                }
                while (!varakozoSzemelyVonatok.TryDequeue(out induloVonat))
                {
                }
            }

            induloVonat.ElozoAllomas = induloVonat.AktualisAllomas;
            induloVonat.KovetkezoAllomas = this.id + induloVonat.Irany;
            induloVonat.AktualisAllomas = -1;

            //sorompok[induloVonat.KovetkezoAllomas].Wait();
        }

        public override string ToString()
        {
            return $"[ {id} ]";
        }
    }
}

