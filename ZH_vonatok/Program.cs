﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZH_vonatok
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Vonat> vonatok = new List<Vonat>()
            {
                new SzemelyVonat(4),
                new SzemelyVonat(4, -1),
                new ExpresszVonat(0),
                new ExpresszVonat(10, -1)
            };

            List<Task> kozlekedes = new List<Task>();
            foreach (var vonat in vonatok)
            {
                kozlekedes.Add(new Task(() => 
                {
                    while (true)
                    {
                        // vonat az állomásra érkezik
                        Vasuthalozat.Allomasok[vonat.AktualisAllomas].ErkezikVonat(vonat);

                        // soromó leenged
                        if (vonat.Irany == 1)
                        { 
                            Vasuthalozat.Sinparok.Where(x => x.BalAllomas == vonat.AktualisAllomas).SingleOrDefault().Sorompo.Release();
                        }
                        if (vonat.Irany == -1)
                        {
                            Vasuthalozat.Sinparok.Where(x => x.JobbAllomas == vonat.AktualisAllomas).SingleOrDefault().Sorompo.Release();
                        }

                        // fel/leszállás
                        if (!Vasuthalozat.Allomasok[vonat.AktualisAllomas].ExpresszMegalloE && vonat is ExpresszVonat)
                        {

                        }
                        else
                        {
                            Thread.Sleep(vonat.FelLeSzallasiIdo); 
                        }

                        // ha végállomáson van
                        if (vonat.AktualisAllomas == 0 || vonat.AktualisAllomas == Vasuthalozat.Allomasok.Length)
                        {
                            vonat.Irany *= -1;
                        }

                        // indulás de a express elől és sorompó fel
                        if (Vasuthalozat.Allomasok[vonat.AktualisAllomas].IndulVonat(vonat)) // express elől
                        {
                            if (vonat.Irany == 1)
                            {
                                Vasuthalozat.Sinparok.Where(x => x.JobbAllomas == vonat.AktualisAllomas).SingleOrDefault().Sorompo.Wait();
                            }
                            if (vonat.Irany == -1)
                            {
                                Vasuthalozat.Sinparok.Where(x => x.BalAllomas == vonat.AktualisAllomas).SingleOrDefault().Sorompo.Wait();
                            }
                        }

                        // vonat úton
                        vonat.KovetkezoAllomas += vonat.Irany;
                        vonat.AktualisAllomas = 0;
                        Thread.Sleep(vonat.AllomasKoztiMenetIdo);
                    }
                }));
            }

            kozlekedes.Add(new Task(() => 
            {
                while (true)
                {
                ToConsole(vonatok);

                }
            }));

            foreach (var t in kozlekedes)
            {
                t.Start();
            }

            Console.ReadKey();
        }

        static void ToConsole(List<Vonat> vonatok)
        {
            while (true)
            {
                Console.Clear();
                Console.SetCursorPosition(0, 0);

                string megallok = "";
                for (int i = 0; i < Vasuthalozat.Allomasok.Length; i++)
                {
                    megallok += Vasuthalozat.Allomasok[i].ToString() + "---";
                }

                Console.WriteLine(megallok);

                int index = 0;

                foreach (var v in vonatok)
                {
                    index++;
                    Console.CursorTop = index;
                    if (v.AktualisAllomas == 0)
                    {
                        Console.CursorLeft = (v.KovetkezoAllomas + 3 )*v.Irany;
                    }
                    else
                    {
                        Console.CursorLeft = v.AktualisAllomas * 3;
                    }

                    Console.WriteLine(v);
                }

                Thread.Sleep(20);
            }
        }
    }
}
