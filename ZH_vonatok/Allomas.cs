﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZH_vonatok
{
    class Allomas
    {
        string nev;
        bool expresszMegalloE;
        ConcurrentQueue<ExpresszVonat> varakozoExpresszVonatok;
        ConcurrentQueue<SzemelyVonat> varakozoSzemelyVonatok;

        public Allomas(string nev, bool expresszMegalloE = false)
        {
            this.Nev = nev;
            this.ExpresszMegalloE = expresszMegalloE;
            varakozoExpresszVonatok = new ConcurrentQueue<ExpresszVonat>();
            VarakozoSzemelyVonatok = new ConcurrentQueue<SzemelyVonat>();
        }

        public string Nev { get => nev; private set => nev = value; }
        public bool ExpresszMegalloE { get => expresszMegalloE; private set => expresszMegalloE = value; }
        public ConcurrentQueue<ExpresszVonat> VarakozoExpresszVonatok { get => varakozoExpresszVonatok; private set => varakozoExpresszVonatok = value; }
        public ConcurrentQueue<SzemelyVonat> VarakozoSzemelyVonatok { get => varakozoSzemelyVonatok; private set => varakozoSzemelyVonatok = value; }

        public void ErkezikVonat(Vonat erkezoVonat)
        {
            if (erkezoVonat is ExpresszVonat && expresszMegalloE)
            {
                varakozoExpresszVonatok.Enqueue(erkezoVonat as ExpresszVonat);
            }
            if (erkezoVonat is SzemelyVonat)
            {
                varakozoSzemelyVonatok.Enqueue(erkezoVonat as SzemelyVonat);
            }
        }

        public bool IndulVonat(Vonat induloVonat)
        {
            bool indulhat = false;

            if (induloVonat is ExpresszVonat)
            {
                ExpresszVonat expresszVonat = induloVonat as ExpresszVonat;
                while (varakozoExpresszVonatok.TryDequeue(out expresszVonat))
                {
                }
                indulhat = true;
            }
            if (induloVonat is SzemelyVonat)
            {
                SzemelyVonat szemelyVonat = induloVonat as SzemelyVonat;
                while (!indulhat)
                {
                    if (varakozoExpresszVonatok.IsEmpty)
                    {
                        indulhat = VarakozoSzemelyVonatok.TryDequeue(out szemelyVonat); //blokkolódik?
                    }
                }
            }
            return indulhat;
        }

        public override string ToString()
        {
            return this.Nev;
        }
    }
}
