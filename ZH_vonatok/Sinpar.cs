﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZH_vonatok
{
    class Sinpar
    {
        int balAllomas;
        int jobbAllomas;
        SemaphoreSlim sorompo;

        public Sinpar(int balAllomas, int jobbAllomas, int sinparokSzama)
        {
            this.BalAllomas = balAllomas;
            this.JobbAllomas = jobbAllomas;
            Sorompo = new SemaphoreSlim(sinparokSzama);
        }

        public SemaphoreSlim Sorompo { get => sorompo; private set => sorompo = value; }
        public int BalAllomas { get => balAllomas; private set => balAllomas = value; }
        public int JobbAllomas { get => jobbAllomas; private set => jobbAllomas = value; }
    }
}
