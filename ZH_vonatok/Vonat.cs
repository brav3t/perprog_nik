﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZH_vonatok
{
    class Vonat
    {
        static Random rnd = new Random();

        int irany;
        int keses;
        int allomasKoztiMenetIdo;
        int felLeSzallasiIdo;
        int aktualisAllomas;
        int kovetkezoAllomas;

        public Vonat(int aktualisAllomas, int irany = 1)
        {
            keses = rnd.Next(0, 2001);
            AllomasKoztiMenetIdo = 5000 + keses;
            FelLeSzallasiIdo = 2000 + keses;
            this.AktualisAllomas = aktualisAllomas;
            this.irany = irany;
        }

        public int Irany { get => irany; set => irany = value; }
        public int FelLeSzallasiIdo { get => felLeSzallasiIdo; private set => felLeSzallasiIdo = value; }
        public int AllomasKoztiMenetIdo { get => allomasKoztiMenetIdo; private set => allomasKoztiMenetIdo = value; }
        public int AktualisAllomas { get => aktualisAllomas; set => aktualisAllomas = value; }
        public int KovetkezoAllomas { get => kovetkezoAllomas; set => kovetkezoAllomas = value; }
    }

    class SzemelyVonat : Vonat
    {
        public SzemelyVonat(int aktualisAllomas, int irany = 1) : base(aktualisAllomas, irany)
        {
        }

        public override string ToString()
        {
            return "Sz";
        }
    }

    class ExpresszVonat : Vonat
    {
        public ExpresszVonat(int aktualisAllomas, int irany = 1) : base(aktualisAllomas, irany)
        {
        }

        public override string ToString()
        {
            return "Ex";
        }
    }
}
