﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZH_vonatok
{
    static class Vasuthalozat
    {
        static Allomas[] allomasok = new Allomas[10] 
        {
            new Allomas("Budapest", true),
            new Allomas("Érd"),
            new Allomas("Velence"),
            new Allomas("Gárdony"),
            new Allomas("Székesfehérvár", true),
            new Allomas("Szabadbattyán"),
            new Allomas("Polgárdi"),
            new Allomas("Lepsény", true),
            new Allomas("Balatonaliga"),
            new Allomas("Siófok", true)
        };

        static Sinpar[] sinparok = Enumerable.Range(0, 9).Select(x => new Sinpar(x, x + 1, (x < 5) ? 2 : 1)).ToArray();

        public static Allomas[] Allomasok { get => allomasok; private set => allomasok = value; }
        public static Sinpar[] Sinparok { get => sinparok; private set => sinparok = value; }
    }
}
