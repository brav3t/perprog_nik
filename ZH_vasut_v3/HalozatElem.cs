﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZH_vasut_v3
{
    abstract class HalozatElem
    {
        static int ID = 0;

        public int Id;

        public HalozatElem()
        {
            this.Id = ID;
            ID++;
        }
        
        public HalozatElem BalElem { get; set; }
        public HalozatElem JobbElem { get; set; }
        public BlockingCollection<Vonat> VonatokElemen { get; set; }

        public abstract void VonatErkezik(Vonat erkezovonat);
        public abstract void VonatIndul();
    }

    class Allomas : HalozatElem
    {
        public string Nev { get; }
        public bool ExpresszMegallo { get; }
        public bool Vegallomas { get; }
        BlockingCollection<Vonat> ExpresszParkolo { get; }

        public Allomas(string nev, bool expresszMegalloE = false, bool vegallomasE = false) : base()
        {
            this.Nev = nev;
            this.ExpresszMegallo = expresszMegalloE;
            this.Vegallomas = vegallomasE;
            ExpresszParkolo = new BlockingCollection<Vonat>();
        }

        public override void VonatErkezik(Vonat erkezovonat)
        {
            if (this.Vegallomas)
            {
                erkezovonat.JobbraIrany = !erkezovonat.JobbraIrany;
            }

            if (erkezovonat is SzemelyVonat)
            {
                VonatokElemen.Add(erkezovonat);
                erkezovonat.VonatHelye = this;
                Thread.Sleep(erkezovonat.VarakozasiIdo);
            }

            if (erkezovonat is ExpresszVonat)
            {
                if (this.ExpresszMegallo)
                {
                    ExpresszParkolo.Add(erkezovonat);
                    erkezovonat.VonatHelye = this;
                    Thread.Sleep(erkezovonat.VarakozasiIdo);
                }
                else
                {
                    erkezovonat.VonatHelye = erkezovonat.JobbraIrany ? JobbElem : BalElem;
                }
            }
        }

        public override void VonatIndul()
        {
            Vonat induloVonat;
            while (ExpresszParkolo.Count > 0)
            {
                induloVonat = ExpresszParkolo.Take();
                induloVonat.VonatHelye = induloVonat.JobbraIrany ? JobbElem : BalElem;
            }
            induloVonat = VonatokElemen.Take();
            induloVonat.VonatHelye = induloVonat.JobbraIrany ? JobbElem : BalElem;
        }
    }

    class Sinpar : HalozatElem
    {
        public Sinpar() : base()
        {
            if (this.Id <= 8)
            {
                VonatokElemen = new BlockingCollection<Vonat>(2);
            }
            else
            {
                VonatokElemen = new BlockingCollection<Vonat>(1);
            }
        }

        public override void VonatErkezik(Vonat erkezovonat)
        {
            this.VonatokElemen.Add(erkezovonat);
            Thread.Sleep(erkezovonat.KozlekedesiIdo);
            this.VonatIndul();
        }

        public override void VonatIndul()
        {
            this.VonatokElemen.Take();
        }
    }
}
