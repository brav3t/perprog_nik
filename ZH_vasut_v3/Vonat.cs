﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZH_vasut_v3
{
    abstract class Vonat
    {
        static Random rnd = new Random();

        public Vonat(HalozatElem vonatKezdoHely, bool jobbraIrany = true)
        {
            this.VonatHelye = vonatKezdoHely;
            this.JobbraIrany = jobbraIrany;
        }

        public HalozatElem VonatHelye { get; set; }
        public bool JobbraIrany { get; set; }
        public int Keses { get => rnd.Next(0, 2001); }
        public int KozlekedesiIdo { get => 5000 + Keses;  }
        public int VarakozasiIdo { get => 2000 + Keses; }
    }

    class SzemelyVonat : Vonat
    {
        public SzemelyVonat(HalozatElem vonatKezdoHely, bool jobbraIrany = true) : base(vonatKezdoHely, jobbraIrany)
        {
        }
    }

    class ExpresszVonat : Vonat
    {
        public ExpresszVonat(HalozatElem vonatKezdoHely, bool jobbraIrany = true) : base(vonatKezdoHely, jobbraIrany)
        {            
        }
    }
}
