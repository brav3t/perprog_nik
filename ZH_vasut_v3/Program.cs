﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZH_vasut_v3
{
    class Program
    {
        static void Main(string[] args)
        {
            HalozatElem[] vasuthalozat = new HalozatElem[19]
            {
                new Allomas("Budapest", true, true),
                new Sinpar(),
                new Allomas("Érd"),
                new Sinpar(),
                new Allomas("Velence"),
                new Sinpar(),
                new Allomas("Gárdony"),
                new Sinpar(),
                new Allomas("Székesfehérvár", true),
                new Sinpar(),
                new Allomas("Szabadbattyan"),
                new Sinpar(),
                new Allomas("Polgárdi"),
                new Sinpar(),
                new Allomas("Lepsény", true),
                new Sinpar(),
                new Allomas("Balatonaliga"),
                new Sinpar(),
                new Allomas("Siófok", true, true)
            };

            List<Vonat> vonatok = new List<Vonat>()
            {
                new SzemelyVonat(vasuthalozat[8]),
                new SzemelyVonat(vasuthalozat[8], false),
                new ExpresszVonat(vasuthalozat[0]),
                new ExpresszVonat(vasuthalozat[19], false)
            };

            List<Task> tasks = new List<Task>();

            foreach (var v in vonatok)
            {

            }

            tasks.Add(new Task(() => 
            {
                ToConsole(vonatok);
            }));

            foreach (var t in tasks)
            {
                t.Start();
            }

            Console.ReadKey();
        }

        static void ToConsole(List<Vonat> vonatok)
        {

        }
    }
}
